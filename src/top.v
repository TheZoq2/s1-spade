module top (
            output wire D3,  // LED on S1 popout
            output wire D7
           );


    // Configure internal HS oscillator as 24MHz
	SB_HFOSC #(.CLKHF_DIV("0b01"))  osc(.CLKHFEN(1'b1),     // enable
  										.CLKHFPU(1'b1), // power up
  										.CLKHF(clk)         // output to sysclk
                                        ) /* synthesis ROUTE_THROUGH_FABRIC=0 */;

    reg rst = 1;

    always @(posedge(clk)) begin
        rst = 0;
    end

    e_proj_main_main uut
        ( .clk_i(clk)
        , .rst_i(rst)
        , .output__({D3, D7})
        );

    // assign D3 = port[2]; // LED should toggle every 400mS
endmodule
